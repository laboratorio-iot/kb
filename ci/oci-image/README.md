# OCI image CI/CD pipeline reference

## Usage

Include pipeline file in the target CI file as follows:

```yaml
include:
  - { project: "laboratorio-iot/kb", file: "ci/oci-image/pipeline.yml" }
```

The following pipeline will be created or added to the current one:

```mermaid
graph LR
  prepare(["Prepare (oci)"])
  lint(["Lint Dockerfile (oci)"])
  build(["Build and Push (oci)"])
  analyze(["Image Quality (oci)"])
  prepare --> lint --> build --> analyze
```

Jobs are internally linked by `needs` keyword so pipeline meets [DAG][dag],
being possible to run in parallel, or even chain to other pipelines.

[dag]: https://docs.gitlab.com/ee/ci/directed_acyclic_graph/

## Image naming schema

One of the purposes of this CI pipeline template is to define an image naming
convention/schema, in order to reduce as much as possible the human overhead
and the possibility of pushing an image with the incorrect name and/or tag.

Four main cases has been taken into account:

- Default name (`Dockerfile`) under default working folder (".")
- Variant name (`Dockerfile.var`) under default working folder (".")
- Default name (`Dockerfile`) under non-default working folder ("folder")
- Variant name (`Dockerfile.var`) under non-default working folder ("folder")

It is not a problem to use folder + subfolder, but when used as part of image
name, any slashes will be replaced by dashes.

Any other case should be avoided as it is not supported or tested (like to
have `Dockerfile-variant`: the 'variant' part will be missed as it is not an
extension).

### Names and tags created for *development builds*

1. Every time an OCI image pipeline finishes, if suceeded, built images will
   be pushed to the registry under the path `builds`
2. Branch name is appended to previous result in a slugged form. Example:
   `main` becomes `builds/main`, while `feat/my-branch` is translated to
   `builds/feat-my-branch`
3. If using a different working dir, it will first slugged, then appended to
   the previous result after a dot ("."). Example: `my/sub-folder` becomes
   `builds/main.my-sub-folder` or `builds/feat-my-branch.my-sub-folder`.
   - It is required to use something different than a slash due to internal
     limitation of no more than 3 level depth. By slugging and adding the dot
     is not possible to reach that limit. Dot instead of *[option here]* due
     to its low chance to appear as a part of a branch name or folder.
4. Tag will be the SHA hash of the involved git commit
5. If using a variant, it will be appended after a dash.

In code, assuming all possible parts, it would be:

```bash
NAME=${CI_REGISTRY_IMAGE}/builds/${CI_COMMIT_REF_SLUG}.${OCI_WORK_DIR_SLUG}
TAG=${CI_COMMIT_SHA}-${DOCKERFILE_EXTENSION}"
```

Each of the previous four cases, reproduced as simple to complex examples:

- Applied to `main` (and `folder`)
  - `registry.gitlab.com/group/project/builds/main:SHA-HASH`
  - `registry.gitlab.com/group/project/builds/main:SHA-HASH-variant`
  - `registry.gitlab.com/group/project/builds/main.folder:SHA-HASH`
  - `registry.gitlab.com/group/project/builds/main.folder:SHA-HASH-variant`
- Applied to `my/branch` (and `my/folder`)
  - `registry.gitlab.com/group/project/builds/my-branch:SHA-HASH`
  - `registry.gitlab.com/group/project/builds/my-branch:SHA-HASH-variant`
  - `registry.gitlab.com/group/project/builds/my-branch.my-folder:SHA-HASH`
  - `registry.gitlab.com/group/project/builds/my-branch.my-folder:SHA-HASH-variant`

### Names and tags created for *release builds*

1. If a pipeline is triggered because a git tag event, if the involved tag
   follows semantic version rules, and if the previous step suceeded, built
   images will be pushed again to the registry as **release images**, using
   the assigned registry name as the base (no `builds` part involved).
2. If using a different working dir, it will first slugged, then appended to
   the previous name. Example: `my/sub-folder` becomes `my-sub-folder`.
3. Image tag will be the git tag itself, as-is.
4. If using a variant, it will be appended after a dash.

In code, assuming all possible parts, it would be:

```bash
NAME="${CI_REGISTRY_IMAGE}/${OCI_WORK_DIR_SLUG}
TAG=${CI_COMMIT_TAG}-${DOCKERFILE_EXTENSION}";
```

Each of the previous four cases, applied to a release:

- `registry.gitlab.com/group/project:SHA-HASH`
- `registry.gitlab.com/group/project:SHA-HASH-variant`
- `registry.gitlab.com/group/project/folder:SHA-HASH`
- `registry.gitlab.com/group/project/folder:SHA-HASH-variant`

> NOTE: Avoid to use a working dir named `builds` as it can create headaches.

## Pipeline jobs

### `Prepare (oci)`

#### Description

- Validates `OCI_WORK_DIR` variable: Cannot be blank
- Validates folder defined by `OCI_WORK_DIR` (relative to project root)
- Compute and prepare all required variables for the rest of the pipeline
  - This was done every time before all jobs, now it is done once and saved
    as environment file, reused later in any job that depends on it.
- Downloads OCI image template files to a temporal folder and:
  - Use default `.hadolint.yml` if not present (relative to `OCI_WORK_DIR`)
  - Use default `.dive.yml` if not present (relative to `OCI_WORK_DIR`)
- Save dotenv file for all other jobs with the following variables:
  - Project basic
    - `OCI_WORK_DIR_SLUG` Relative path to folder inside project root, slug
      version, safe to add as URL/registry paths
  - Image building flow/procedure
    - `OCI_BUILD_DATE_RFC3339` Date with OCI-compliant format RFC3339
  - Image naming schema: Common
    - `OCI_IMAGE_TAG_SUFFIX` Suffix to add to an image tag (extracted from
       the extension of the Dockerfile, if found. No suffix otherwise)
  - Image naming schema: Development build (all pipelines)
    - `OCI_IMAGE_NAME` Composed image name, ready to be used
    - `OCI_IMAGE_TAG` Composed image tag, ready to be used
    - `OCI_IMAGE_FQN` Image fully qualified name (name:tag[-suffix])
    - `OCI_IMAGE_FQN_LATEST` Latest image fully qualified name (name:latest[-suffix])
  - Image naming schema: Release build (only if semver-compliant git tag)
    - `OCI_IMAGE_REL_FQN` Image fully qualified name (name:tag[-suffix])
    - `OCI_IMAGE_REL_FQN_LATEST` Latest image fully qualified name (name:latest[-suffix])
  - Image naming schema: Development build for automation (all pipelines)
    - `OCI_IMAGE_BY_PIPE_FQN` Pipeline based image fully qualified name
      (registry/pipeline:PIPELINE_ID)

#### Generated artifacts

- Linter and analyzer config files to be used in the following jobs
- dotenv file with all required environment variables as download
- dotenv file is also saved as 'report' so it is seeded to the following jobs

```yaml
paths:
  - ${OCI_WORK_DIR}/.hadolint.yml
  - ${OCI_WORK_DIR}/.dive.yml
  - ${OCI_WORK_DIR}/oci-image-build.env
reports:
  dotenv: ${OCI_WORK_DIR}/oci-image-build.env
```

### `Lint Dockerfile (oci)`

#### Description

- Validates configured `Dockerfile` using `hadolint` tool
- Linter options can be configured using [`.hadolint.yml`][linter-config]
  - Job will always pass, but it shows as warning if linting failed

[linter-config]: https://gitlab.com/laboratorio-iot/kb/-/blob/main/ci/oci-image/.hadolint.yml

#### Generated artifacts

- Generates `hadolint-codeclimate-report.json` (CodeClimate-compliant quality
  report)
  - As downloadable artifact
  - As GitLab report: Available in pipeline UI as 'Code Quality' tab

```yaml
paths:
  - ${OCI_WORK_DIR}/hadolint-codeclimate-report.json
reports:
  codequality:
    - ${OCI_WORK_DIR}/hadolint-codeclimate-report.json
```

### `Build and Push (oci)`

#### Description

- Build the image according all previous configuration
  - It uses `docker build` right now, but it will change depending the needs
- Job will fail if build process or tag or push actions fails at some point
- If succeeded, two tags will be generated and pushed to project registry,
  named **Development build**
- Easy registry maintenance: Registry is considered clean as soon as images
  matching `/builds/*` are deleted (no other project should rely on this ones
  as it can be deleted at any time with no prior advice).
- If ran as a `tag` pipeline job, and the tag follows semantic version rules,
  two more tags are created and pushed, named **Release build**

#### Generated artifacts

- This job does not generate any downloadable artifact (as per definition)
- The form of generated artifact are OCI images pushed to container registry.
  Once saved there, a `docker pull` will be able get them (auth may apply).

### `Image Quality (oci)`

#### Description

- Analyzes the generated image using `dive` tool
- Analyzer options can be configured using [`.dive.yml`][analyzer-config]
  - Job will always pass, but it shows as warning if quality is below the
    configured limits

[analyzer-config]: https://gitlab.com/laboratorio-iot/kb/-/blob/main/ci/oci-image/.dive.yml

#### Generated artifacts

- Generates `dive-image-report.json` (Parseable information about the image)
  - As downloadable artifact
  - Not yet directly supported by GitLab interface
- Part of the analysis is the efficiency, calculated as the amount of space
  the image is not wasting
  - This value is captured as `coverage`, so it can be later used

## Configuration

> **NOTE:** This project follows [convention over configuration][conv-config]
> pattern, so the pipeline is already configured and ready to go with no
> further user interaction, more than including the pipeline file itself.
>
> Any change in the environment variables, other than the proposed below, is
> not recommended and should be avoided.

[conv-config]: https://en.wikipedia.org/wiki/Convention_over_configuration

If required, the following pipeline configuration variables are available:

```yaml
# BEWARE: Do not let 'variables' block with no content.
#         It will not fail, but it will break inheritance.
variables:
  # Path relative to the project folder where the job have to be ran.
  # Under this path is where files like Dockerfile, .hadolint.yml or .dive.yml
  # are expected. This value will be also used to compose the image name (so
  # one repository can contain more than a single image without name clash).
  # If no relative path is desired, let it as a dot (".") or commented
  # Examples: ".", "folder", "folder/subfolder"
  OCI_WORK_DIR: "."

  # Any extra argument according 'docker build' documentation. It has to be
  # provided this way to make it cross-compatible between different builders
  # (like docker buildx, buildah, podman, kaniko, ...).
  # Examples: "--build-arg OCI_USER_UID --label maintainer='Name <email>'"
  OCI_BUILD_EXTRA_ARGS: ""

  # File with Dockerfile-compatible content to be used to build the image.
  # It is located or relative to OCI_ROOT_DIR/OCI_WORK_DIR.
  # Examples: "relative/path/Dockerfile", "Dockerfile.variant"
  OCI_BUILD_DOCKERFILE: "Dockerfile"

  # Folder that holds the content to be used to build the image.
  # The content of this folder is sent to the builder and used to build the
  # image. This means that any content outside this folder cannot be used or
  # referenced during the building stage. Use "." to use the current folder.
  # It is located or relative to OCI_ROOT_DIR/OCI_WORK_DIR.
  # Examples: "relative/path"
  OCI_BUILD_CONTEXT: "."
```

## Development

Definitions file is included by:

```yaml
include:
  - { project: "laboratorio-iot/kb", file: "ci/oci-image/defs.yml" }
```

The following jobs are added and disabled by default (prefixed with a dot):

### `.oci-image-stages`

Instead of using any of the GitLab pre-defined stages (named `.pre`, `build`,
`test`, `deploy` or `.post`), proposed stages uses the same name as the job
they runs to ease matching by visual inspection. Because they are unique, any
possible multi-pipeline name clash is avoided.

As `stages` cannot be extended in any way, the three GitLab's default stages
(`build`, `test`, `deploy`) are also added. This way is easy to chain jobs to
this pipeline without the need of a child pipeline (and making it harder to
get artifacts).

Overall stages when referencing `.oci-image-stages`:

| Stage name              | Description                                     |
| :---------              | :----------                                     |
| `.pre`                  | Not used, harcoded by GitLab                    |
| `Prepare (oci)`         | Dedicated to `Prepare (oci)` job                |
| `Lint Dockerfile (oci)` | Dedicated to `Lint Dockerfile (oci)` job        |
| `Build and Push (oci)`  | Dedicated to `Build and Push (oci)` job         |
| `Image Quality (oci)`   | Dedicated to `Image Quality (oci)` job          |
| `.post`                 | Not used, hardcoded by GitLab                   |

### `.oci-image-variables`

Set of predefined variables required to be globally available. Already
explained in [Configuration](#Configuration) section.

### `.oci-image-prepare`

Enabled as job `Prepare (oci)` at `pipeline.yml`

### `.oci-image-base-job`

It contains only a `before_script` block and is meant to be extended by the
real job definitions.

By extending it, the working directory is changed to the resolved one.

### `.oci-image-lint-dockerfile`

Enabled as job `Lint Dockerfile (oci)` at `pipeline.yml`

### `.oci-image-build-and-push`

Enabled as job `Build and Push (oci)` at `pipeline.yml`

### `.oci-image-analyze`

Enabled as job `Image Quality (oci)` at `pipeline.yml`

## Authors

- CieNTi <cienti@cienti.com>
