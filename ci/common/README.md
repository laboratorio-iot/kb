# Common files

Pipeline files defining the lowest-level resources (in order of importance):

1. `runners.defs.yml`: All available runners defined by workload and not tags
2. `stages.yml`: Pre-defined set of recommended stages
3. `workflows.defs.yml`: Pre-defined set of recommended rules and workflows
4. `jobs.yml`: Extra utility jobs that are out of a single pipeline

## Stages

Pre-defined set of recommended stages. It conforms a pretty generic pipeline stages where all other pipelines should fit.

If some specific stage is required, end-user must ensure that any expected stage is also included, because as soon as `stages` keyword is used it will fully replace whatever is previously defined.

```mermaid
graph LR
  subgraph Pre-flight
    Pre-flight_Job(["Jobs"])
  end
  subgraph Pre-build
    Pre-build_Job(["Jobs"])
  end
  subgraph Build
    Build_Job(["Jobs"])
  end
  subgraph Test
    Test_Job(["Jobs"])
  end
  subgraph Review
    Review_Job(["Jobs"])
  end
  subgraph Security
    Security_Job(["Jobs"])
  end
  subgraph Deploy
    Deploy_Job(["Jobs"])
  end
  subgraph Post-deploy
    Post-deploy_Job(["Jobs"])
  end
  subgraph Post-flight
    Post-flight_Job(["Jobs"])
  end
  Pre-flight_Job --> Pre-build_Job --> Build_Job --> Test_Job --> Review_Job --> Security_Job --> Deploy_Job --> Post-deploy_Job --> Post-flight_Job
```

### Pre-flight stage

Environment checks and preparations

### Pre-build stage

Anything required before `Build` stage (linting, cache preparation, credentials ...)

### Build stage

Jobs that usually work together with repository content at a specific commit, and generate some important content (artifacts, images, packages, ...)

### Test stage

Using `Build` stage artifacts, along with other cases, perform all required tests to validate the previous stages

### Review stage

Execute automated or manual inspection based on previous stages. Similar to `Deploy`, but less restrictive, it also assumes any *Review App* alike approach (start environment, route map, review, stop environment)

### Security stage

Test any unsafe behaviour before going to `Deploy` stage (secrets leaking, docker vulnerability, code audit, ...)

### Deploy stage

Jobs that actually publish what was previously tested (like Pages). If infrastructure, it should follow Test -> Staging -> Production chain to be fully safe.

### Post-deploy stage

Similar to `Pre-build`, anything to be executed once the deployment stage is finished

### Post-flight stage

Similar to `Pre-flight`, anything to be executed just before to finish
