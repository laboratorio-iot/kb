# Información general

## Preparación del entorno de desarrollo local

El entorno de desarrollo estará contenido en una imagen/contenedor de [Docker](https://www.docker.com/). Esto permitirá que se parta de un entorno pre-fijado y controlado, evitando por tanto errores entre versiones o instalaciones innecesarias.

### Maquina virtual

Muy resumidamente, Docker es un sistema de virtualización que permite arrancar lo que se llaman *contenedores*. Dentro de estos contenedores se ejecutará generalmente un Linux extremadamente reducido, con los paquetes y programas necesarios para hacer una tarea concreta.

Debido a limitaciones de Windows y el uso de USB, no es posible desarrollar la practica completa usando solo Windows y Docker: Se pueden compilar programas para la placa, pero no se pueden flashear/subir a la placa por dichos problemas con los puertos USB.

Este problema recurrente en Windows se salva mediante máquinas virtuales con Linux instalado, como se ha podido comprobar en diferentes situaciones. Para este caso, se usará la versión de Ubuntu con escritorio y ligera `Lubuntu 20.04`, que se proporcionará con todo lo necesario para arrancar la práctica.

### Entorno virtual

Lo explicado en el punto anterior indica el uso de 2 sistemas diferentes de virtualizacion: Maquina virtual y Docker. Esto puede generar confusión si no se ejecutan ciertos comandos dentro del entorno adecuado, pero es extremadamente sencillo una vez se entiende estos detalles.

Las diferentes capas, por tanto quedan algo como:

1. El sistema operativo principal actuará como sistema anfitrión, por lo general Windows, donde se ejecutará una maquina virtual. Si se necesita intercambiar ficheros entre el ordenador y la maquina virtual, se recomienda hacerlo mediante el uso de carpetas compartidas (en la propia configuración de la maquina virtual).
2. Una vez arrancada la maquina virtual, habrá partes que sean gráficas y otras que requieran del uso de la consola o terminal. El icono para abrirla se encuentra tanto en los menus, como en la barra de inicio, al lado del icono de Firefox.
3. Dentro de esta consola, al ejecutar `iotlab` se accederá a la consola 'final', donde reside todo el entorno de desarrollo junto a todo lo necesario para programar la placa de NRF52840. *Este comando puede fallar si no se ha clonado correctamente el repositorio*

### Instalación en Windows. VirtualBox

En el documento '[Preparación de la maquina virtual: VirtualBox](./docs/es/prepare-vm_virtualbox.md)' se explica paso a paso el proceso de creación de la máquina virtual.

Una vez se sigan los pasos, y quede terminado el bloque, es posible continuar al clonado (descarga) del repositorio.

## Preparación de la carpeta de trabajo

### Importación de aliases

Para facilitar la configuración y ejecucion del contenedor de trabajo se instalará un script, llamado `updater`, encargado de descargar y mantener lo necesario. Para instalarlo se ejecutará lo siguiente en una consola:

```bash
wget -O /tmp/updater https://laboratorio-iot.gitlab.io/kb/updater; bash /tmp/updater install
```

Una vez termine, si no hubo errores se cierra y abre de nuevo la consola para que termine de aplicar los cambios.

### Crear grupo personal en Gitlab

Cada persona creará un grupo dentro de su grupo de trabajo ([Link al actual '2021 - 2022'](https://gitlab.com/laboratorio-iot/grupos/2021-2022)) usando el botón de la siguiente figura:

![Crear subgrupo](./docs/img/workspace-setup_new-user-group.png)

El nombre de grupo será el nombre de la persona, y la parte editable de la URL del grupo, será el identificador de usuario (sin la '@'). En la siguiente figura se muestra un ejemplo y donde leer rápidamente el identificador de usuario:

![Detalles subgrupo](./docs/img/workspace-setup_new-user-group-details.png)

Una vez creado, deberá quedar una dirección URL con el identificador de usuario integrado en ella, que según el ejemplo de la figura anterior sería: `https://gitlab.com/laboratorio-iot/grupos/2021-2022/cienti`.

> **NOTA**: En caso de equivocación casi todo es editable, y en el peor de los casos es cuestion de borrar y empezar de nuevo. Pregunta

El grupo permite separar el trabajo de cada persona, y entre otras cosas, que no colisionen nombres y se pueda mantener `iotlab` de forma individual, como se indica a continuación.

## Dongles usados para las prácticas

### Nordic Semiconductors *nRF52840 Dongle (PCA10059)*

#### Información basica

- [Página oficial del dongle](https://www.nordicsemi.com/Software-and-tools/Development-Kits/nRF52840-Dongle)
- [Manual de Usuario oficial](https://gitlab.com/laboratorio-iot/kb/-/package_files/23915003/download)
- [Ficheros de Hardware](https://gitlab.com/laboratorio-iot/kb/-/package_files/23914977/download)

#### Pinout

![Pinout del dongle de Nordic](./docs/img/nordicsemi-donble_pinout.png)

## Información complementaria

- [Comandos de `git` para las prácticas](./docs/es/git-cheatsheet.md)

## Authors

- CieNTi <cienti@cienti.com>
