---
title: 'My presentation'
subtitle: 'Subtitle of the presentation'
author: 'CieNTi'
version: '0.0.1'
date: 'December 2021'
theme: 'metropolis'
#themestyle: 'light'
colortheme: 'default'
fonttheme: 'Nord'
#fontfamily: 'FiraSans'
mainfont: 'Yanone Kaffeesatz'
sansfont: 'Andika New Basic'
monofont: 'DejaVu Sans Mono'
aspectratio: 169
header-includes:
  - \usepackage{cmbright}
fontsize: 10pt
---

# Outline

- Big picture
- Recent work and status
- Ongoing work and status
- Future work
