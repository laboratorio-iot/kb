---
title: 'nRF Connect SDK and Zephyr RTOS'
subtitle: 'Use for NRF52840 Dongle devices'
author: 'CieNTi'
version: '0.0.2'
date: 'December 2021'
theme: 'metropolis'
#themestyle: 'light'
colortheme: 'default'
fonttheme: 'Nord'
#fontfamily: 'FiraSans'
mainfont: 'Yanone Kaffeesatz'
sansfont: 'Andika New Basic'
monofont: 'DejaVu Sans Mono'
aspectratio: 169
header-includes:
  - \usepackage{cmbright}
fontsize: 10pt
---

# Context

![Context view. (Source: se.ewi.tudelft.nl)](./nrf-connect-sdk/zephyr-context.png)

---

# nRF Connect

![nRF Connect Ecosystem. (Source: zephyr.org)](./nrf-connect-sdk/nrf-connect-ecosystem.png)

---

# Architecture

![The Zephyr system architecture. (Source: zephyr.org)](./nrf-connect-sdk/./architecture.png)

---

# Application tree

![Application tree structure. (Source: se.ewi.tudelft.nl)](./nrf-connect-sdk/app-tree.png)

---

# Tree examples

\begincols
\begincol{0.40\textwidth}

![Simple: Blinky](./nrf-connect-sdk/blinky-tree.png)

\endcol
\begincol{0.56\textwidth}

![With extras: Alexa Gadget](./nrf-connect-sdk/alexa-tree.png)

\endcol
\endcols

---

# nRF Connect SDK: Tree

\begincols
\begincol{0.48\textwidth}

![nRF Connect SDK tree - 1](./nrf-connect-sdk/ncs-tree-1.png)

\endcol
\begincol{0.48\textwidth}

![nRF Connect SDK tree - 2](./nrf-connect-sdk/ncs-tree-2.png)

\endcol
\endcols

---

# nRF Connect SDK: Documentation

![nRF Connect SDK - Main documentation page](./nrf-connect-sdk/1640175505.png)

\tiny Link: [https://developer.nordicsemi.com/nRF_Connect_SDK/doc/1.8.0/nrf/index.html](https://developer.nordicsemi.com/nRF_Connect_SDK/doc/1.8.0/nrf/index.html)

---

# nRF Connect SDK: Version 1.8.0

![nRF Connect SDK - Select version](./nrf-connect-sdk/1640175591.png)

\tiny Link: [https://developer.nordicsemi.com/nRF_Connect_SDK/doc/1.8.0/nrf/index.html](https://developer.nordicsemi.com/nRF_Connect_SDK/doc/1.8.0/nrf/index.html)

---

# nRF Connect SDK: Frameworks

![nRF Connect SDK - Select framework](./nrf-connect-sdk/1640175632.png)

\tiny Link: [https://developer.nordicsemi.com/nRF_Connect_SDK/doc/1.8.0/nrf/index.html](https://developer.nordicsemi.com/nRF_Connect_SDK/doc/1.8.0/nrf/index.html)

---

# nRF Connect SDK: Zephyr RTOS 2.7.0 (1.8.0)

![Zephyr version - Related to 'latest'](./nrf-connect-sdk/1640175844.png)

\tiny Link: [https://developer.nordicsemi.com/nRF_Connect_SDK/doc/1.8.0/zephyr/index.html](https://developer.nordicsemi.com/nRF_Connect_SDK/doc/1.8.0/zephyr/index.html)

---

# nRF Connect SDK: Zephyr RTOS 2.7.99 (latest)

![Zephyr version - Related to 'latest' also](./nrf-connect-sdk/1640175894.png)

\tiny Link: [https://developer.nordicsemi.com/nRF_Connect_SDK/doc/latest/zephyr/index.html](https://developer.nordicsemi.com/nRF_Connect_SDK/doc/latest/zephyr/index.html)

---

# Examples: Zephyr RTOS

![Bluetooth examples from Zephyr](./nrf-connect-sdk/1640177301.png)

\tiny Link: [https://developer.nordicsemi.com/nRF_Connect_SDK/doc/1.8.0/zephyr/samples/bluetooth/bluetooth.html](https://developer.nordicsemi.com/nRF_Connect_SDK/doc/1.8.0/zephyr/samples/bluetooth/bluetooth.html)

---

# Examples: nRF connect SDK

![Bluetooth examples from NCS](./nrf-connect-sdk/1640177270.png)

\tiny Link: [https://developer.nordicsemi.com/nRF_Connect_SDK/doc/1.8.0/nrf/samples/samples_bl.html](https://developer.nordicsemi.com/nRF_Connect_SDK/doc/1.8.0/nrf/samples/samples_bl.html)

---

# Kconfig reference

![Kconfig reference](./nrf-connect-sdk/1640177328.png)

\tiny Link: [https://developer.nordicsemi.com/nRF_Connect_SDK/doc/1.8.0/kconfig/index.html](https://developer.nordicsemi.com/nRF_Connect_SDK/doc/1.8.0/kconfig/index.html)

---

# Kconfig reference: Zephyr RTOS

![Kconfig reference - Zephyr specific](./nrf-connect-sdk/1640177419.png)

\tiny Link: [https://developer.nordicsemi.com/nRF_Connect_SDK/doc/1.8.0/kconfig/index-zephyr.html](https://developer.nordicsemi.com/nRF_Connect_SDK/doc/1.8.0/kconfig/index-zephyr.html)

---

# Kconfig reference: nRF Connect SDK

![Kconfig reference - nRF specific](./nrf-connect-sdk/1640177446.png)

\tiny Link: [https://developer.nordicsemi.com/nRF_Connect_SDK/doc/1.8.0/kconfig/index-nrf.html](https://developer.nordicsemi.com/nRF_Connect_SDK/doc/1.8.0/kconfig/index-nrf.html)

# Kconfig reference: On-chip temperature (1)

![Config options example: On-chip temperature](./nrf-connect-sdk/1640178311.png)

\tiny Link: [https://developer.nordicsemi.com/nRF_Connect_SDK/doc/1.8.0/kconfig/index-all.html](https://developer.nordicsemi.com/nRF_Connect_SDK/doc/1.8.0/kconfig/index-all.html)

# Kconfig reference: On-chip temperature (2)

![Dedicated information about `CONFIG_TEMP_NRF5`](./nrf-connect-sdk/1640178341.png)

\tiny Link: [https://developer.nordicsemi.com/nRF_Connect_SDK/doc/1.8.0/kconfig/CONFIG_TEMP_NRF5.html#std-kconfig-CONFIG_TEMP_NRF5](https://developer.nordicsemi.com/nRF_Connect_SDK/doc/1.8.0/kconfig/CONFIG_TEMP_NRF5.html#std-kconfig-CONFIG_TEMP_NRF5)
