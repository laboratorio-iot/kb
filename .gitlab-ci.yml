# DESCRIPTION
#   Preconfigured generic pipeline. Write jobs and go!
#
# AUTHORS
#   CieNTi <cienti@cienti.com>
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Add required modules (https://gitlab.com/laboratorio-iot/kb/-/tree/main/ci)
include:
  - { project: "laboratorio-iot/kb", file: "ci/common/generic-pipeline.yml" }

variables:
  JOB_BASE_IMAGE:
    value: "registry.gitlab.com/laboratorio-iot/kb/hub/alpine:3.15"
    description: "Base OCI image to be used on each job (if no other is defined)"

  FEAT_DEBUG_CI:
    value: disabled
    description: "Set to 'enable' or 'disable' to execute debug jobs or not"

  FEAT_BUILD_SLIDES:
    value: enabled
    description: "Set to 'enable' or 'disable' to build slides using nicedoc or not"

  FEAT_BUILD_IMAGES:
    value: enabled
    description: "Set to 'enable' or 'disable' to build OCI images or not"

  FEAT_MIRROR_IMAGES:
    value: enabled
    description: "Set to 'enable' or 'disable' to mirror LTS images on hub or not"

Build slides:
  rules:
    - if: $FEAT_BUILD_SLIDES == "enabled"
  stage: Build
  trigger:
    include: slides/.gitlab-ci.yml
    strategy: depend

Build images:
  rules:
    - if: $FEAT_BUILD_IMAGES == "enabled"
  stage: Build
  trigger:
    include: oci/.gitlab-ci.yml
    strategy: depend

Publish to Hub:
  rules:
    - if: $FEAT_MIRROR_IMAGES == "enabled"
  stage: Deploy
  extends:
    - .oci-shell-runner
  image: "${JOB_BASE_IMAGE}"
  script:
    # Some verbosity
    - docker --version;
    # Ensure credentials
    - docker login
        --username ${CI_REGISTRY_USER}
        --password ${CI_REGISTRY_PASSWORD}
        ${CI_REGISTRY};
    # Go for it!
    - cd oci;
    # OS: Alpine
    - ./oci-helper mirror
        alpine:3.15
        registry.gitlab.com/laboratorio-iot/kb/hub/alpine:3.15 || true;
    - ./oci-helper mirror
        registry.gitlab.com/laboratorio-iot/kb/oci/alpine-base:3.15
        registry.gitlab.com/laboratorio-iot/kb/hub/alpine-base:3.15 || true;
    - ./oci-helper mirror
        registry.gitlab.com/laboratorio-iot/kb/oci/alpine-base:3.15-0.0.2
        registry.gitlab.com/laboratorio-iot/kb/hub/alpine-base:3.15-0.0.2 || true;
    # OCI pipeline: dive and hadolint
    - ./oci-helper mirror
        dive:0.10.0
        registry.gitlab.com/laboratorio-iot/kb/hub/dive:0.10.0 || true;
    - ./oci-helper mirror
        hadolint:2.6.0-0-g7160964-alpine
        registry.gitlab.com/laboratorio-iot/kb/hub/hadolint:2.6.0-0-g7160964-alpine || true;
    # IoTLab
    - ./oci-helper mirror
        registry.gitlab.com/laboratorio-iot/grupos/admin/iotlab/root-pipelines:432852153
        registry.gitlab.com/laboratorio-iot/kb/hub/iotlab:0.1.0 || true;
    - ./oci-helper mirror
        registry.gitlab.com/laboratorio-iot/grupos/admin/iotlab:1.1.0
        registry.gitlab.com/laboratorio-iot/kb/hub/iotlab:1.1.0 || true;
    # Nicedoc
    - ./oci-helper mirror
        registry.gitlab.com/laboratorio-iot/kb/oci/nicedoc:0.0.4
        registry.gitlab.com/laboratorio-iot/kb/hub/nicedoc:0.0.4 || true;
    # OS: Ubuntu
    - ./oci-helper mirror
        ubuntu:20.04
        registry.gitlab.com/laboratorio-iot/kb/hub/ubuntu:20.04 || true;
    - ./oci-helper mirror
        ubuntu:21.10
        registry.gitlab.com/laboratorio-iot/kb/hub/ubuntu:21.10 || true;
    - ./oci-helper mirror
        registry.gitlab.com/laboratorio-iot/kb/oci/ubuntu-base:20.04
        registry.gitlab.com/laboratorio-iot/kb/hub/ubuntu-base:20.04 || true;
    - ./oci-helper mirror
        registry.gitlab.com/laboratorio-iot/kb/oci/ubuntu-base:20.04-0.0.2
        registry.gitlab.com/laboratorio-iot/kb/hub/ubuntu-base:20.04-0.0.2 || true;
    - ./oci-helper mirror
        registry.gitlab.com/laboratorio-iot/kb/oci/ubuntu-base:21.10-0.0.3-21.10-impish
        registry.gitlab.com/laboratorio-iot/kb/hub/ubuntu-base:21.10-0.0.3 || true;
  allow_failure: false
  artifacts:
    when: always
    public: false
    expire_in: 1 week
    name: "${CI_PROJECT_NAME}_mirror_${CI_COMMIT_REF_SLUG}-${CI_COMMIT_SHORT_SHA}"
    expose_as: Mirror images to hub
    paths:
      - oci/pulled-images.txt
      - oci/pushed-images.txt

# Deploy to pages, mandatory 'pages' name
pages:
  stage: Deploy
  extends:
    - .oci-image-runner
  image: "${JOB_BASE_IMAGE}"
  script:
    # Fail if error
    - set -e;
    # Save some files to be published
    - mkdir -pv public;
    - cp oci/iotlab/extras/updater public/updater;
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  artifacts:
    paths:
      - "public"
  allow_failure: false
