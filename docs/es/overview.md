# Introducción a nRF52840

## Descripción

La placa `nRF52840 Dongle` es una placa de desarrollo con la circuitería mínima básica para permitir programarla y utilizarla sin hardware ni cables extra.

![Imagen de PCA10059](overview_board-picture.png)

## Requisitos

De forma genérica, para poder trabajar con una placa basada en microcontroladores, se necesita:

- Placa que admita (re)programación
  - Mediante programador externo (ej.: JLink, OpenOCD)
  - Mediante algún mecanismo interno (ej.: Bootloader por serie o aire)
- Software que se comunica con programador o bootloader
- Toolchains y SDK
  - Permiten compilar un código para la placa
  - Librerías de periféricos o funcionalidades
  - Ejemplos

Para el caso concreto de la placa `nRF52840 Dongle`:

- Placa que admite (re)programación
  - Bootloader que permite entrar en modo `DFU` (Device Firmware Upgrade)
  - Se pulsa el botón lateral, el led rojo parpadea suavemente y listo
- Software que se comunica con el bootloader
  - `nrfutil` hace esta función
  - Permite construir el paquete .zip
  - Permite flashearlo
- Toolchains y SDK (+ SoftDevice)
  - `gcc para ARM`: Toolchain que soporta, entre otros, nRF51 y nRF52
  - `nRF SDK`: Da soporte a la mayoria de chips de las familias nRF51 y nRF52
  - `nRF Command Line Tools`: Requerido por Nordic, contiene `nrfjprog` y `mergehex`

Aunque todo esto se da preinstalado en la imagen de Docker, es necesario conocerlo ya que en mayor o menor medida se necesitará a la hora de querer diseñar y programar una aplicación.

## SDK

### Familias de SDKs

Nordic proporciona dos SDK principales, que no se deben confundir:

- `nRF5 SDK`: Recomendado para nRF51/nRF52
- `nRF Connect SDK`: Recomendado para nRF91/nRF53

Al tratarse de placas basadas en `nRF52840`, se usará `nRF5 SDK`.

### Familias de nRF5 SDK

Nordic de nuevo proporciona algunas variantes, a destacar:

- `nRF5 SDK`: Familia básica. Debe usarse cuando el resto no apliquen. Soporta BLE y ANT
- `nRF5 SDK for Bluetooth Mesh`
- `nRF5 SDK for Thread and ZigBee`

Hay otras 4 variantes (`Homekit`, `Thingy:52`, `nRFready Smart Remote 3` y `nRFready Desktop 2`) que se descartan por requerir permisos especiales o no tener relación.

### nRF5 SDK

Kit de Desarrollo de Software: Incluye todos los ficheros necesarios para dar soporte a las familias nRF51/nRF52.

Se proporciona preinstalado dentro del contenedor en la ruta `/opt/nRF5_SDK`.

Muestra de su contenido:

```plaintext
├── components
│   ├── ant
│   ├── ble
│   ├── boards
│   ├── libraries
│   ├── softdevice
│   └── toolchain
├── config
│   └── nrf52840
├── documentation
├── examples
│   ├── ble_peripheral
│   ├── dfu
│   └── peripheral
├── external
│   ├── cJSON
│   ├── freertos
│   ├── lwip
│   ├── mbedtls
│   └── nfc_adafruit_library
├── external_tools
├── integration
└── modules
```

### SDK vs. SoftDevice

- SDK sirve para dar soporte a familias de micros
- SoftDevice da soporte a protocolos inalámbricos
- Están relacionados y debe usarse la versión de SoftDevice indicada en el SDK
- Para ello, el SDK incluye las versiones que acepta

## SoftDevice

### Descripción

Software precompilado y linkado que implementa uno o varios protocolos wireless, desarrollados por Nordic Semiconductor (aunque tambien los hay de terceros, como ANT).

Al ser software precompilado el uso es casi inmediato: seleccionar cual se quiere usar y programarlo.

Para que todo funcione correctamente el programa que se ejecute en la placa debe estar preparado para el SoftDevice seleccionado.

La siguiente figura ilustra la arquitectura de un sistema funcional:

![Bloques funcionales - SoftDevice](overview_softdevice.png)

### Reglas de nombrado

Para saber reconocer que hace o a que familia aplica el SoftDevice, se siguen dos reglas principales:

1. El SoftDevice se compone de dos pilas de protocolo: BLE y ANT
   - BLE tiene dos roles: Central (Master) y Peripheral (Slave)
   - Se usa el formato `Sxyz` donde:
     - `x`: Tipo de protocolo. 1 para BLE, 2 para ANT, 3 para BLE y ANT combinados
     - `y`: Rol de BLE. 1 para Slave, 2 para Master, 3 para Master y Slave combinados
     - `z`: Familia del SoC. 0 para nRF51, 2 o 3 para nRF52
   - Ejemplos:
     - `S130`: BLE que soporta Master y Slave, para nRF51
     - `S212`: ANT, para nRF52
2. Hereda la regla anterior, pero las 2 ultimas cifras se corresponden con las del chip
   - Aplica, entre otros, a nRF52840: Debido a su gran capacidad no es necesario crear diferentes SoftDevices
   - Ejemplos:
     - `S140`: BLE completo para nRF52840 (long range, master, slave, multilink, advertising)
     - `S340`: BLE y ANT para nRF52840

### Versiones incluídas

Localizadas en la carpeta `/opt/nRF5_SDK/components/softdevice`:

| Comunes |   BLE   |   ANT   | BLE/ANT |
| :-----: | :-----: | :-----: | :-----: |
| `common` | `s112` | `s212`  | `s312`  |
| `mbr`    | `s113` |         |         |
|          | `s122` |         |         |
|          | `s132` |         | `s332`  |
|          | `s140` |         | `s340`  |

## Entorno de desarrollo

### Configuración y descarga iniciales

Se agregará al final de `/home/vmu/.bashrc` el siguiente alias:

```bash
# Add contiki 'iotlab' alias
export NRFW_PATH="${HOME}/work";
iotlab()
{
  if [ "$(docker ps -qaf name=iotlab)" = "" ]; then
    echo 'Container not found, creating it ...';
    # Or host networking, or port forwarding:
    # Replace network host argument line by the following two, to create
    # an isolated network (port forward is mandatory)
    #      --sysctl net.ipv6.conf.all.disable_ipv6=0 \
    #      --publish 10080:80 \
    # Devices plugged-in after docker is started are not recognized
    # --volume /dev/bus/usb:/dev/bus/usb \
    docker run -it \
      --name iotlab \
      --privileged \
      --volume "${NRFW_PATH}:/workdir" \
      --env DISPLAY=$DISPLAY \
      --volume /tmp/.X11-unix:/tmp/.X11-unix \
      --privileged \
      --volume /dev:/dev \
      registry.gitlab.com/laboratorio-iot/kb/hub/iotlab:latest \
    ;
  else
    if [ "${1}" = "restart" ] && ! docker stop iotlab > /dev/null; then
      echo 'Error while stopping the container, exiting now ...';
      return 1;
    fi;
    if ! docker start iotlab > /dev/null; then
      echo 'Error while starting the container, exiting now ...';
      return 1;
    fi;
    echo 'Container found and running, executing a shell ...';
    docker exec -it iotlab bash --login;
  fi;
};
```

> **NOTA:** Este cambio no será efectivo hasta que no se vuelva a abrir el terminal.

Para arrancar el entorno con todo lo referente a nRF52840 preinstalado solo queda ejecutar el comando `iotlab`.

### Programacion

Conforme se compile la aplicacion, usando `make`, se podrá pasar a flashear la placa como sigue:

```bash
nrfutil pkg generate --hw-version 52 --sd-req 0x00 --application-version 4 --application _build/nrf52840_xxaa.hex _build/dfu.zip;
nrfutil dfu usb-serial -pkg _build/dfu.zip -p /dev/ttyACM0 -b 115200;
```

## TBD

### MBR, DFU, APP, SoftDevice

![mbr-dfu-app-sd](overview_mbr-dfu-app-sd.png)

## Resumen de enlaces

### General

- [nRF52840 dongle Home](https://www.nordicsemi.com/Products/Development-hardware/nRF52840-Dongle/GetStarted)
- Get Started
  - [Getting started with the nRF52840 Dongle](https://infocenter.nordicsemi.com/index.jsp?topic=%2Fug_nrf52840_dongle%2FUG%2Fnrf52840_Dongle%2Fgetting_started.html)
  - Getting started guide
    - [Nordic Tools and downloads](https://infocenter.nordicsemi.com/topic/ug_getting_started/UG/common/nordic_tools.html?cp=1_0_1)
    - [Available protocols](https://infocenter.nordicsemi.com/topic/ug_getting_started/UG/gs/avail_protocols.html?cp=1_0_0)
    - [Software development](https://infocenter.nordicsemi.com/topic/ug_getting_started/UG/gs/develop_sw.html?cp=1_0_2)
- Relevant SDKs
  - [nRF Connect SDK](https://www.nordicsemi.com/Products/Development-software/nRF-Connect-SDK)
  - [nRF5 SDK (BLE and ANT)](https://www.nordicsemi.com/Products/Development-software/nRF5-SDK)
    - [Documentation](https://infocenter.nordicsemi.com/topic/struct_sdk/struct/sdk_nrf5_latest.html)
  - [nRF5 SDK for Mesh](https://www.nordicsemi.com/Products/Development-software/nRF5-SDK-for-Mesh)
    - [Documentation](https://infocenter.nordicsemi.com/topic/struct_sdk/struct/sdk_mesh_latest.html)
  - [nRF5 SDK for Thread and Zigbee (multiprotocol adds BLE)](https://www.nordicsemi.com/Products/Development-software/nRF5-SDK-for-Thread-and-Zigbee)
    - [Documentation](https://infocenter.nordicsemi.com/topic/struct_sdk/struct/sdk_thread_zigbee_latest.html)

### Resumen `Nordic Tools and Downloads`

- Development IDE: Se usa **GNU/GCC**
- Essential tools
  - [SDK (Software Development Kit)](https://developer.nordicsemi.com/)
  - [nRF Command Line Tools](https://www.nordicsemi.com/Software-and-Tools/Development-Tools/nRF-Command-Line-Tools/Download#infotabs)
    - [Documentation](https://infocenter.nordicsemi.com/topic/ug_nrf_cltools/UG/cltools/nrf_command_line_tools_lpage.html)
- Optional tools
  - SoftDevice: Wireless protocol stack
    - [Downloads for nRF52840](https://www.nordicsemi.com/Products/Low-power-short-range-wireless/nRF52840/Compatible-downloads#infotabs)
    - [Documentation for nRF52](https://infocenter.nordicsemi.com/topic/struct_nrf52/struct/nrf52_softdevices.html)
  - [nRF Connect for Desktop](https://www.nordicsemi.com/Software-and-Tools/Development-Tools/nRF-Connect-for-desktop): Expandable desktop tool with several apps
    - [Downloads](https://www.nordicsemi.com/Software-and-Tools/Development-Tools/nRF-Connect-for-desktop/Download#infotabs)
    - [Documentation](https://infocenter.nordicsemi.com/topic/ug_nrfconnect_ble/UG/nRF_Connect_BLE/nRF_Connect_intro.html)
  - [nRF Connect for Mobile](https://www.nordicsemi.com/Software-and-Tools/Development-Tools/nRF-Connect-for-mobile): Peer device emulator app for smartphones
    - [Downloads for Android](https://play.google.com/store/apps/details?id=no.nordicsemi.android.mcp)
    - [Downloads for IOS](https://apps.apple.com/us/app/nrf-connect/id1054362403)
  - [Nordic nRF Toolbox app](https://www.nordicsemi.com/Software-and-Tools/Development-Tools/nRF-Toolbox): App that contains all the Nordic apps
    - [Downloads for Android](https://play.google.com/store/apps/details?id=no.nordicsemi.android.nrftoolbox&hl=en)
    - [Downloads for IOS](https://apps.apple.com/us/app/nrf-toolbox/id820906058?mt=8)
    - [Downloads for Windows Phone](https://github.com/NordicPlayground/Windows-nRF-Toolbox)
  - [nRF Sniffer](https://www.nordicsemi.com/Software-and-Tools/Development-Tools/nRF-Sniffer-for-Bluetooth-LE/)
    - [Download page](https://www.nordicsemi.com/Software-and-Tools/Development-Tools/nRF-Sniffer-for-Bluetooth-LE/Download#infotabs)
    - [Documentation](https://infocenter.nordicsemi.com/topic/ug_sniffer_ble/UG/sniffer_ble/intro.html)
  - [nRF Thread Topology Monitor](https://www.nordicsemi.com/Software-and-Tools/Development-Tools/nRF-Thread-topology-monitor)
    - [Download page](https://www.nordicsemi.com/Software-and-Tools/Development-Tools/nRF-Thread-topology-monitor/Download#infotabs)
    - [Documentation](https://infocenter.nordicsemi.com/topic/ug_nrf_ttm/UG/nrf_ttm/ttm_introduction.html)
  - Thread Border Router: Gateway for connecting Thread network to the Internet
    - [Download page](https://www.nordicsemi.com/Software-and-Tools/Software/nRF5-SDK-for-Thread-and-Zigbee/Download#infotabs)
    - [Documentation](https://infocenter.nordicsemi.com/topic/sdk_tz_v4.1.0/thread_border_router.html)
  - [Nordic mobile apps](https://www.nordicsemi.com/Software-and-Tools/Development-Tools/Mobile-Apps)
