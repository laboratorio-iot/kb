# Contiki-ng para NRF52840

## Preparación del entorno

### Importar repositorio de trabajo en Gitlab

Cada persona va a crear una copia de este repositorio dentro de su carpeta personal, y para facilitar la tarea (además de mantener las relaciones entre repositorios), se hará mediante el método llamado `fork`.

Para esto, lo primero será entrar en [Contiki-NG](https://gitlab.com/laboratorio-iot/grupos/res/contiki-ng) e ir a la sección de fork, como se muestra en la imagen:

![Botón de fork de Contiki-NG](./docs/img/workspace-setup_fork-button-location.png)

La pantalla siguiente mostrará los posibles emplazamientos donde GitLab permite copiarlo, y debería aparecer el grupo recien creado, como se ve en la figura. Es fácil confirmarlo ya que debe verse la etiqueta `Owner` tras el nombre (lo que indica control total sobre ese grupo, tanto de creación, como de gestión, como de borrado).

![Nuevo fork de Contiki-NG](./docs/img/workspace-setup_new-fork-options.png)

### Siguiente paso

Cada persona debe tener ya su repositorio personal: Este contiene todo el código de Contiki necesario para las prácticas, aunque por ahora está solo en Gitlab.

Para poder trabajar con él es necesario clonarlo en el entorno de desarrollo como sigue.

## Clonado del repositorio en el entorno de desarrollo

A partir de ahora todo el trabajo se hará en la propia máquina virtual y, solo cuando sea necesario, se indicará atender a algo que esté fuera de esta (como las conexiones USB, que se deben habilitar desde VirtualBox).

> **NOTA**: Este procedimiento tambien puede encontrarse en el fichero `cheatsheet.txt` que puede verse en el escritorio de la maquina virtual, de forma que sea fácil copiar y pegar.

Dentro de una consola (terminal) se debe proceder como sigue:

```bash
# Crear la carpeta donde ira contiki, si no existe
mkdir -pv "${HOME}/work/contiki-ng"
# Entrar dentro de la carpeta 'work'
cd "${HOME}/work"
# Clonar el repositorio personal de contiki (editar 'cienti' acorde al identificador de cada persona)
git clone https://gitlab.com/laboratorio-iot/grupos/2021-2022/cienti/contiki-ng.git ${HOME}/work/contiki-ng/
# Entrar dentro de la carpeta de contiki
cd "${HOME}/work/contiki-ng"
# Descargar todos los submodulos necesarios
git submodule update --init --recursive
```

Una vez terminado el proceso se puede pasar a comprobar la programación, mediante el test inicial.

## Test inicial: `Hello World`

Dentro de una consola se ejecuta el comando `iotlab`, que abrirá una sesión con el entorno virtual de Contiki-NG bajo Docker, viendose algo similar a:

```bash
user@a939004e869d:~/contiki-ng$
```

El hecho de que aparezca `user@xxxx` indica que todos los comandos se ejecutarán dentro del entorno de desarrollo virtual, y que ademas ya está localizado dentro de la carpeta de trabajo `contiki-ng`.

Si no fuera asi, y siempre que se quiera volver a la carpeta de trabajo de contiki, podremos hacerlo mediante cualquiera de los siguientes comandos:

```bash
cd ~/contiki-ng
o
cd $HOME/contiki-ng
o
cd /home/user/contiki-ng
```

Si cualquiera de estos 3 comandos falla, es muy probable que no se esten ejecutando dentro del sistema virtual dockerizado (`iotlab`).

Una vez dentro, se compila el ejemplo de hello-world como sigue:

```bash
cd examples/hello-world
make
```

Si la compilación termina sin problemas, se puede pasar a flashear/programar la placa. Esto se hará con agregando el sufijo `.dfu-upload` como sigue:

```bash
make hello-world.dfu-upload
```

Si no hay problemas, se debe ver algo parecido a:

```bash
Zip created at build/nrf52840/dongle/nrf52840_dfu_image.zip
nrfutil dfu usb-serial -p /dev/ttyACM0 -pkg build/nrf52840/dongle/nrf52840_dfu_image.zip
  [####################################]  100%
Device programmed.
```

Y si los hay, lo primero es confirmar que el dongle se encuentra en modo `DFU`:

- Dongle de Makerdiary: Desenchufar el dongle, pulsar y mantener USR/RST mientras se enchufa de nuevo, esperar 2/3 segundos y soltar el pulsador.
- Dongle de Nordicsemi: En este caso no hay necesidad de desenchufar y bastará con presionar el pulsador RST.

> Si el led rojo comienza a encenderse y apagarse de forma gradual, **ya está en modo DFU**.

Para comprobar el puerto serie (VCOM), despues de flashear correctamente ejecutamos:

```bash
picocom -fh -b 115200 --imap lfcrlf /dev/ttyACM0
```

Si se recibe la cadena de texto `Hello, world` de forma periódica es que todo ha ido bien. Para salir de picocom se debe presionar `Ctrl + a` y luego `Ctrl + x` (sin soltar la tecla `Ctrl` es mas cómodo).
