#!/usr/bin/printf ERROR: This file is meant to be source'd. Exit now ..%.s\n

# Add Contiki NG-ready 'contiker' alias
# Arguments:
#   - none: Start a session (will create it if not present)
#   - restart: Stop and start (previous config when 'run' apply)
#   - rebuild: Stop, delete and run (new 'run' config apply)
export CNG_PATH="${HOME}/work/contiki-ng";
mkdir -p "${CNG_PATH}";
contiker()
{
  _name="contiker";
  _image="registry.gitlab.com/laboratorio-ris/contiki-ng:latest";
  _cont_presence="$(docker ps --quiet --all --filter name="${_name}")";
  if ! [ "${_cont_presence}" = "" ]; then
    if [ "${1}" = "restart" ] || [ "${1}" = "rebuild" ]; then
      echo "[${_name} launcher] Container found, stoping it ...";
      docker stop --time 5 "${_name}";
    fi;
    if [ "${1}" = "rebuild" ]; then
      echo "[${_name} launcher] Removing container ...";
      docker rm --force "${_name}";
      _cont_presence="";
    fi;
  fi;

  # Second 'if' with same condition than before because 'rebuild' can change it
  if [ "${_cont_presence}" = "" ]; then
    echo "[${_name} launcher] Creating new container ...";
    # Or host networking, or port forwarding:
    # Replace network host argument line by the following two, to create
    # an isolated network (port forward is mandatory)
    #      --sysctl net.ipv6.conf.all.disable_ipv6=0 \
    #      --publish 10080:80 \
    # Devices plugged-in after docker is started are not recognized
    # --volume /dev/bus/usb:/dev/bus/usb \
    docker pull "${_image}" || true;
    if ! docker run -it \
      --name "${_name}" \
      --privileged \
      --mount type=bind,source=$CNG_PATH,destination=/home/user/contiki-ng \
      --env DISPLAY=$DISPLAY \
      --network host \
      --volume "${HOME}/.gitconfig:/user/vmu/.gitconfig" \
      --volume /tmp/.X11-unix:/tmp/.X11-unix \
      --device=/dev/bus/usb \
      --device=/dev/usb \
      "${_image}";
    then
      echo "[${_name} launcher] Error while running the container, exiting now ...";
      return 1;
    fi;
  else
    echo "[${_name} launcher] Starting container ...";
    if ! docker start "${_name}" > /dev/null; then
      echo "[${_name} launcher] Error while starting the container, exiting now ...";
      return 1;
    fi;
    echo "[${_name} launcher] Container found and running, executing a shell ...";
    docker exec -it "${_name}" bash --login;
  fi;
  return 0;
};

# Add nRF Connect-ready 'iotlab' alias
# Arguments:
#   - none: Start a session (will create it if not present)
#   - restart: Stop and start (previous config when 'run' apply)
#   - rebuild: Stop, delete and run (new 'run' config apply)
export NRFW_PATH="${HOME}/work/iotlab";
mkdir -p "${NRFW_PATH}";
iotlab()
{
  _name="iotlab";
  _image="registry.gitlab.com/laboratorio-iot/kb/oci/iotlab:latest";
  _cont_presence="$(docker ps --quiet --all --filter name="${_name}")";
  if ! [ "${_cont_presence}" = "" ]; then
    if [ "${1}" = "restart" ] || [ "${1}" = "rebuild" ]; then
      echo "[${_name} launcher] Container found, stoping it ...";
      docker stop --time 5 "${_name}";
    fi;
    if [ "${1}" = "rebuild" ]; then
      echo "[${_name} launcher] Removing container ...";
      docker rm --force "${_name}";
      _cont_presence="";
    fi;
  fi;

  # Second 'if' with same condition than before because 'rebuild' can change it
  if [ "${_cont_presence}" = "" ]; then
    echo "[${_name} launcher] Creating new container ...";
    # Or host networking, or port forwarding:
    # Replace network host argument line by the following two, to create
    # an isolated network (port forward is mandatory)
    #      --sysctl net.ipv6.conf.all.disable_ipv6=0 \
    #      --publish 10080:80 \
    docker pull "${_image}" || true;
    if ! docker run -it \
      --name "${_name}" \
      --group-add "$(getent group dialout | cut -d: -f3)" \
      --device-cgroup-rule='c 166:* rmw' \
      --volume "${HOME}/.gitconfig:/home/user/.gitconfig" \
      --volume "${NRFW_PATH}:/workdir" \
      --volume /run/udev:/run/udev:ro \
      --volume /dev:/dev \
      "${_image}";
    then
      echo "[${_name} launcher] Error while running the container, exiting now ...";
      return 1;
    fi;
  else
    echo "[${_name} launcher] Starting container ...";
    if ! docker start "${_name}" > /dev/null; then
      echo "[${_name} launcher] Error while starting the container, exiting now ...";
      return 1;
    fi;
    echo "[${_name} launcher] Container found and running, executing a shell ...";
    docker exec -it "${_name}" bash --login;
  fi;
  return 0;
};
