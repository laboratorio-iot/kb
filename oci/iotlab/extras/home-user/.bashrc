# .bashrc
cat << EOF;
 ___    _____ _          _        ____            _        _
|_ _|__|_   _| |    __ _| |__    / ___|___  _ __ | |_ __ _(_)_ __   ___ _ __
 | |/ _ \| | | |   / _\` | '_ \  | |   / _ \| '_ \| __/ _\` | | '_ \ / _ \ '__|
 | | (_) | | | |__| (_| | |_) | | |__| (_) | | | | || (_| | | | | |  __/ |
|___\___/|_| |_____\__,_|_.__/   \____\___/|_| |_|\__\__,_|_|_| |_|\___|_|
                                  $(id -n -u)/$(id -n -g) ($(id -u)/$(id -g))
EOF
echo "";
echo "- GNU ARM Toolchain .....: ${GNUARMEMB_TOOLCHAIN_PATH}";
echo "- nRF Connect SDK (ncs) .: ${NCS_ROOT} (extra nRF SDK: ${SDK_ROOT})";
echo "- Zephyr base ...........: ${ZEPHYR_BASE} (Variant: ${ZEPHYR_TOOLCHAIN_VARIANT})";
echo "- Board .................: ${BOARD} (Default port '${PORT}' @ ${BAUDRATE} bps)";
echo "";

# For some reason `/etc/bash.bashrc` decides to overwrite PS1, so recompose it
export PS1="${PS1_A} ${PS1_B} ${PS1_C} ${PS1_D}\n${PS1_E} ";

# Set up the command-line build environment
# https://developer.nordicsemi.com/nRF_Connect_SDK/doc/latest/nrf/gs_installing.html#set-up-the-command-line-build-environment
this_folder="${PWD}";
cd "${NCS_ROOT}";
source zephyr/zephyr-env.sh;
cd "${this_folder}";

# Function to ease the compile + package + flash with nrfutil procedure
# Examples:
#   - For current folder: cd /ncs/zephyr/samples/basic/blinky; bake-it
#   - For different folder: cd /ncs; bake-it zephyr/samples/basic/blinky
bake-it() {
  echo "[Bake-It] Building ...";
  if ! west build -b "${BOARD}" "${1}"; then
    echo "[Bake-It] Build failed, cleaning before retry ...";
    if ! west build -t clean "${1}"; then
      echo "[Bake-It] Clean failed, trying pristine before retry ...";
      # Maybe dirty 'build' folder, do pristine clean before retry
      west build -t pristine;
      west build -t clean "${1}" || return 1;
    fi;
    echo "[Bake-It] Building again, last chance ...";
    west build -b "${BOARD}" "${1}" || return 1;
  fi;
  echo "[Bake-It] Packing ...";
  nrfutil pkg generate \
    --hw-version 52 \
    --sd-req 0x00 \
    --application build/zephyr/zephyr.hex \
    --application-version 1 \
  dfu.zip || return 1;
  echo "[Bake-It] Flashing ...";
  nrfutil dfu usb-serial -pkg dfu.zip -p "${PORT}" || return 1;
}

# Wrapper for picocom to just focus on port and baudrate
# $1 for port, otherwise PORT is used (From Dockerfile)
# $2 for baudrate, otherise BAUDRATE is used (From Dockerfile)
open-it() {
  _PORT="${1}";
  _PORT="${_PORT:-${PORT}}";
  _BAUDRATE="${1}";
  _BAUDRATE="${_BAUDRATE:-${BAUDRATE}}";
  echo "[Open-It] Launching picocom to chat with the device";
  echo "[Open-It] - Port '${_PORT}' at ${_BAUDRATE} bps";
  echo "[Open-It] - Remember: Press CTRL + A then CTRL + X to exit";
  while ! picocom -q -fh -b "${_BAUDRATE}" --imap lfcrlf "${_PORT}"; do
    sleep 0.25;
  done;
  echo "[Open-It] Session finished";
}
