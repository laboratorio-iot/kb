# IoTLab - Development Container

OCI image containing all required packages to correctly perform the exercises

## TL;DR; Usage

### Build the image locally

**NOTE:** This step is only required for development or local tests. For any other case it is recommended to use the published image instead.

```bash
docker build . --tag iotlab:local
```

### Launch a container

The recommended stable image is `registry.gitlab.com/laboratorio-iot/kb/oci/iotlab:latest` (replace it by `iotlab:local` if local build).

```bash
# Fastest: Run it as an isolated application
docker run --rm -it registry.gitlab.com/laboratorio-iot/kb/oci/iotlab:latest

# Recommended: Run it sharing current folder as `workdir` inside the container,
# with the same user as host and dialout group id to gain correct USB rights.
docker run --rm -it \
  --user "$(id -u):$(getent group dialout | cut -d: -f3)" \
  --volume "$(pwd):/workdir" \
  --env DISPLAY="${DISPLAY}" \
  --volume /tmp/.X11-unix:/tmp/.X11-unix \
  --privileged \
  --volume /dev:/dev \
  registry.gitlab.com/laboratorio-iot/kb/oci/iotlab:latest
```

### Do some task

Once the container session is open, the following items are available:

- `app`: App description

As a part of the requisites, it is also included:

- `syspkg`: System package description

## Learn by example

- Example
  - [Holder](./examples/example-holder/README.md)

## Documentation

- [Dealing with USB and Docker](./docs/dealing-with-usb-and-docker.md)
- [Dealing with Linux and Bluetooth](./docs/dealing-with-linux-and-bluetooth.md)
- [Setup Notes - nRF Connect SDK](./docs/setup-notes_nrf-connect-sdk.md)
- [Setup Notes - nRF SDK](./docs/setup-notes_nrf-sdk.md)
- Tested [`.devcontainer.json`](./docs/.devcontainer.json) (partially works, left for future tests)

## Authors

- CieNTi <cienti@cienti.com>
- `scan-for-urls` found in extras folder comes from [Google's Eddystone](https://raw.githubusercontent.com/google/eddystone/master/eddystone-url/implementations/linux/scan-for-urls)
