# Dealing with Linux and Bluetooth

## Current status

There are lot of sites out there teaching how to do tasks related to bluetooth that are outdated and should be avoided.

- **Deprecated tools:** `ciptool`, `gatttool`, `hciattach`, `hciconfig`, `hcidump`, `hcitool`, `rfcomm`, `sdptool`
- **Functionality replaced by:** `bluetoothctl`

Among other sites explaining this, [Fedora notice page](https://fedoraproject.org/wiki/Changes/BluetoothDeprecated) is given as example

## Installing requisites

TODO: It requires `bluez` for sure, but what more?

## Using `bluetoothctl`

### Warning about versions and commands set

As seen after different searches, there are two main versions recognized by the type of commands:

- Commands like `set-scan-filter-clear` appear to be present in versions like 5.42.
- Commands like `menu scan` > `clear` (or shorthand `scan.clear`) are present in 5.62 and appear to be the latest set.
  - Basic help is OK, but detailed is pretty hidden. The best is to refer to sources, like *[Help with `scan.transport`](https://github.com/bluez/bluez/blob/8c0c82c9e966654002350ada8489ca577896566a/doc/adapter-api.txt#L91)*

In `Fedora 34` the present one is 5.62, so the latest command set is used.

### Manual vs. Automatic

Command `bluetoothctl` opens a session and allows the user to introduce commands, making it a great tool to investigate and debug.

For automatic tasks, the same command history has to be repeated, and this can be achieved by something like `echo -e "command1\ncommand2\n" | bluetoothctl` or `bluetoothctl -- command`.

### Menu vs. Command

Once inside a `bluetoothctl` session, actions can be done navigating through menus or executing commands `by path` alike. The following example enable `LE` scan in each way:

 1. By menu:
    1. `menu scan`: Enters `scan` menu
    2. `clear`: Disable all filters and set all defaults
    3. `transport le`: Enable `LE` transport
    4. `back`: Exit `scan` and returns to main menu
 2. By command:
    1. `scan.clear`: Disable all filters and set all defaults
    2. `scan.transport le`: Enable `LE` transport

### Fast recipes

The proposed list of commands are expected to be executed within a `bluetoothctl` session (or being passed to it, like with `echo` or `cat`)

#### Scan `BR/EDR`

```plaintext
power on
scan.clear
scan.transport bredr
scan on
```

#### Scan `LE`

```plaintext
power on
scan.clear
scan.transport le
scan on
```

### Scanning for Eddystone Beacons

## Troubleshooting

### Check hardware and daemon

- [How to debug Bluetooth problems](https://fedoraproject.org/wiki/How_to_debug_Bluetooth_problems)

### Returns one of `NotReady`, `Blocked` or `Busy` error

One recurrent problem is that the adapter get stucked by some operation or program, therefore it cannot be managed. Common scenario is:

- `bluetoothctl scan on` returns `org.bluez.Error.NotReady`
- `bluetoothctl power on` returns `org.bluez.Error.Blocked`
- `bluetoothctl power on` returns `org.bluez.Error.Busy`

To fix this, the adapter has to be unblocked by `rfkill` program.

#### Unblock all

The fast and dirty solution is to `unblock all`, which will affect all wireless adapters found:

```bash
rfkill unblock all
```

There will be no output and the adapter should be ready to be used.

#### Unblock one

If it is required to unblock a single device, list them by `rfkill list` first, then unlock the selected one:

```bash
# List status of all wireless adapters
$ rfkill list
0: hci0: Bluetooth
        Soft blocked: yes
        Hard blocked: no
1: phy0: Wireless LAN
        Soft blocked: no
        Hard blocked: no

# Unblock only bluetooth adapter: hci0
rfkill unblock hci0
```

There will be no output and the adapter should be ready to be used.

#### Power on

Once unblocked, adapter should accept `bluetoothctl power on` and it will be ready to go.

```bash
$ bluetoothctl power on
Changing power on succeeded
```
