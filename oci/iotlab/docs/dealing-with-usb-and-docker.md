# Dealing with USB and Docker

In order to access USB peripherals the user has to has enough rights or be `root`.

Usually, as we focus on serial peripherals, it is only required to add the user to `dialout` group.

The problem arises when host and container `dialout` group id are different, because no matter if container user has enough rights when the device itself is tied to a different group id.

Under the provided VM the problem does not exists, as both id matches, but testing this container on a different linux machine can trigger that error.

To overcome this, the dirty hack is to add the container user to all low-ids (1 to 20), which tipically contains `dialout` group. This way, no matter the name is given to the group, that is likely to work.

Tested commands:

## No privileged, extra group, using `device-cgroup-rule` (best approach)

This method is the best in terms of balance:

- It uses default non-root user and group within container
- Adds host dialout as extra user group
- Thanks to `/run/udev` (read-only), USB changes are reported without restart the container
- Thanks to `/dev` (can be dangerous if using `root`) it is possible to access the device
- Thanks `device-cgroup-rule` host-to-container permissions (udev rules) are fixed
  - Value `166` is taken from [Linux devices](https://github.com/torvalds/linux/blob/master/Documentation/admin-guide/devices.txt#L2371) (ACM USB modems)

```bash
docker run --rm -it \
  --volume "$(pwd):/workdir" \
  --group-add "$(getent group dialout | cut -d: -f3)" \
  --device-cgroup-rule='c 166:* rmw' \
  --volume /run/udev:/run/udev:ro \
  --volume /dev:/dev \
  iotlab:local
```

## No privileged, no user/group mapping, using `device`

This is the safest and recommended way, because by using `device`, `privileged` is not required. Internally in `Dockerfile` the final effective user is `user` (because `root` re-add some unsafety).

```bash
docker run --rm -it \
  --volume "$(pwd):/workdir" \
  --device /dev/bus \
  --device /dev/usb \
  --device /dev/bus/usb \
  iotlab:local
```

This method, as described, does not work because device name does not appear (requires exact match). To fix it:

```bash
docker run --rm -it \
  --volume "$(pwd):/workdir" \
  --device /dev/ttyACM0 \
  iotlab:local
```

This change makes `/dev/ttyACM0` to be present (if it was present at host, otherwise it fails), but still has permissions errors due to the group id mismatch.

## No privileged, with exact user/group mapping, using `device`

This is at the same safety level, but it requires the user to get the `dialout` group id instead of its own:

```bash
docker run --rm -it \
  --user "$(id -u):$(getent group dialout | cut -d: -f3)" \
  --volume "$(pwd):/workdir" \
  --device /dev/ttyACM0 \
  iotlab:local
```

This **works** perfectly, but it requires the boards to be connected before running the container and to adapt the command to one or more ports (not valid for the `iotlab` as command approach).

## Privileged mounting buses

Instead of `device`, the buses can be mounted inside the container, but it requires privileged access along with exact `dialout` id (if non-root):

```bash
docker run --rm -it \
  --user "$(id -u):$(getent group dialout | cut -d: -f3)" \
  --volume "$(pwd):/workdir" \
  --privileged \
  --volume /dev/bus:/dev/bus \
  --volume /dev/usb:/dev/usb \
  iotlab:local
```

This **works** perfectly, but it requires the boards to be connected before running the container (valid for the `iotlab` but not perfect).

## Privileged mounting `/dev` as non-root

Instead of the buses, full `/dev` can be mounted inside the container, which also requires privileged access along with exact `dialout` id (if non-root):

```bash
docker run --rm -it \
  --user "$(id -u):$(getent group dialout | cut -d: -f3)" \
  --volume "$(pwd):/workdir" \
  --privileged \
  --volume /dev:/dev \
  iotlab:local
```

This **works** perfectly, and also **recognizes board connection changes** (almost perfect `iotlab`, because the unsafety ... but for now is enough as it does not require root).

## Privileged mounting `/dev` as `root`

Repeating the previous case, full `/dev` is mounted inside the container, which requires privileged access, but this time is the `root` moment:

```bash
docker run --rm -it \
  --user 0:0 \
  --volume "$(pwd):/workdir" \
  --privileged \
  --volume /dev:/dev \
  iotlab:local
```

This **works** perfectly, and also **recognizes board connection changes** (less perfect for `iotlab` than the one before, because the extra unsafety ... but it serves as the last resort).
