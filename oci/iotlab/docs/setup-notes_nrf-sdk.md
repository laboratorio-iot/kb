# Setup notes for nRF SDK

- nRF52840 dongle is also known as `PCA10059`.
- Examples are expected to be in the nRF5 SDK, under `PCA10059`

## Links from Nordic site

- [nRF52840 dongle Home](https://www.nordicsemi.com/Products/Development-hardware/nRF52840-Dongle/GetStarted)
- Get Started
  - [Getting started with the nRF52840 Dongle](https://infocenter.nordicsemi.com/index.jsp?topic=%2Fug_nrf52840_dongle%2FUG%2Fnrf52840_Dongle%2Fgetting_started.html)
  - Getting started guide
    - [Nordic Tools and downloads](https://infocenter.nordicsemi.com/topic/ug_getting_started/UG/common/nordic_tools.html?cp=1_0_1)
    - [Available protocols](https://infocenter.nordicsemi.com/topic/ug_getting_started/UG/gs/avail_protocols.html?cp=1_0_0)
    - [Software development](https://infocenter.nordicsemi.com/topic/ug_getting_started/UG/gs/develop_sw.html?cp=1_0_2)
- Relevant SDKs
  - [nRF5 SDK (BLE and ANT)](https://www.nordicsemi.com/Products/Development-software/nRF5-SDK)
    - [Documentation](https://infocenter.nordicsemi.com/topic/struct_sdk/struct/sdk_nrf5_latest.html)
  - [nRF5 SDK for Mesh](https://www.nordicsemi.com/Products/Development-software/nRF5-SDK-for-Mesh)
    - [Documentation](https://infocenter.nordicsemi.com/topic/struct_sdk/struct/sdk_mesh_latest.html)
  - [nRF5 SDK for Thread and Zigbee (multiprotocol adds BLE)](https://www.nordicsemi.com/Products/Development-software/nRF5-SDK-for-Thread-and-Zigbee)
    - [Documentation](https://infocenter.nordicsemi.com/topic/struct_sdk/struct/sdk_thread_zigbee_latest.html)

### Following `Get Started`

To get a general overview, each item is visited and described as follows.

#### From `Getting started with the nRF52840 Dongle`

Based on `nRF Connect for Desktop` which is not yet the case (not inside `iotlab` image)

#### From `Nordic Tools and downloads`

- Development IDE
  - Discarded options: SEGGER's SES, MDK-ARM uVision and IAR
    - GUI or private/licensed
  - Accepted option: **GNU/GCC**
    - Command line and public
- Essential tools
  - [SDK (Software Development Kit)](https://developer.nordicsemi.com/): **THE** download site, it explains how to install and use the SDK.
  - [nRF Command Line Tools](https://www.nordicsemi.com/Software-and-Tools/Development-Tools/nRF-Command-Line-Tools/Download#infotabs)
    - [Documentation](https://infocenter.nordicsemi.com/topic/ug_nrf_cltools/UG/cltools/nrf_command_line_tools_lpage.html)
- Optional tools
  - SoftDevice: Wireless protocol stack
    - [Downloads for nRF52840](https://www.nordicsemi.com/Products/Low-power-short-range-wireless/nRF52840/Compatible-downloads#infotabs)
    - [Documentation for nRF52](https://infocenter.nordicsemi.com/topic/struct_nrf52/struct/nrf52_softdevices.html)
  - [nRF Connect for Desktop](https://www.nordicsemi.com/Software-and-Tools/Development-Tools/nRF-Connect-for-desktop): Expandable desktop tool with several apps
    - [Downloads](https://www.nordicsemi.com/Software-and-Tools/Development-Tools/nRF-Connect-for-desktop/Download#infotabs)
    - [Documentation](https://infocenter.nordicsemi.com/topic/ug_nrfconnect_ble/UG/nRF_Connect_BLE/nRF_Connect_intro.html)
  - [nRF Connect for Mobile](https://www.nordicsemi.com/Software-and-Tools/Development-Tools/nRF-Connect-for-mobile): Peer device emulator app for smartphones
    - [Downloads for Android](https://play.google.com/store/apps/details?id=no.nordicsemi.android.mcp)
    - [Downloads for IOS](https://apps.apple.com/us/app/nrf-connect/id1054362403)
  - [Nordic nRF Toolbox app](https://www.nordicsemi.com/Software-and-Tools/Development-Tools/nRF-Toolbox): App that contains all the Nordic apps
    - [Downloads for Android](https://play.google.com/store/apps/details?id=no.nordicsemi.android.nrftoolbox&hl=en)
    - [Downloads for IOS](https://apps.apple.com/us/app/nrf-toolbox/id820906058?mt=8)
    - [Downloads for Windows Phone](https://github.com/NordicPlayground/Windows-nRF-Toolbox)
  - [nRF Sniffer](https://www.nordicsemi.com/Software-and-Tools/Development-Tools/nRF-Sniffer-for-Bluetooth-LE/)
    - [Download page](https://www.nordicsemi.com/Software-and-Tools/Development-Tools/nRF-Sniffer-for-Bluetooth-LE/Download#infotabs)
    - [Documentation](https://infocenter.nordicsemi.com/topic/ug_sniffer_ble/UG/sniffer_ble/intro.html)
  - [nRF Thread Topology Monitor](https://www.nordicsemi.com/Software-and-Tools/Development-Tools/nRF-Thread-topology-monitor)
    - [Download page](https://www.nordicsemi.com/Software-and-Tools/Development-Tools/nRF-Thread-topology-monitor/Download#infotabs)
    - [Documentation](https://infocenter.nordicsemi.com/topic/ug_nrf_ttm/UG/nrf_ttm/ttm_introduction.html)
  - Thread Border Router: Gateway for connecting Thread network to the Internet
    - [Download page](https://www.nordicsemi.com/Software-and-Tools/Software/nRF5-SDK-for-Thread-and-Zigbee/Download#infotabs)
    - [Documentation](https://infocenter.nordicsemi.com/topic/sdk_tz_v4.1.0/thread_border_router.html)
  - [Nordic mobile apps](https://www.nordicsemi.com/Software-and-Tools/Development-Tools/Mobile-Apps)

#### From `Available protocols`

List all supported protocols in a table and has links to each of them.

#### From `Software development`

There are download links for the following SDKs:

- [nRF5 SDK](https://developer.nordicsemi.com/nRF5_SDK/)
- [nRF5 SDK for HomeKit](https://www.nordicsemi.com/Software-and-Tools/Software/nRF5-SDK-for-HomeKit)
- [nRF5 SDK for Thread and ZigBee](https://www.nordicsemi.com/Software-and-Tools/Software/nRF5-SDK-for-Thread-and-Zigbee)
- [nRF5 SDK for Mesh](https://www.nordicsemi.com/Software-and-Tools/Software/nRF5-SDK-for-Mesh)

Extra links:

- [GitHub profile](https://github.com/NordicSemiconductor)
- [nRF Sniffer](https://www.nordicsemi.com/Software-and-Tools/Development-Tools/nRF-Sniffer-for-Bluetooth-LE/)
- [Tutorials](https://devzone.nordicsemi.com/nordic/)
  - [Guides for nRF5 SDK](https://devzone.nordicsemi.com/guides/short-range-guides/): **THE** practical information zone
    - [nRF52840 Dongle Programming Tutorial](https://devzone.nordicsemi.com/guides/short-range-guides/b/getting-started/posts/nrf52840-dongle-programming-tutorial)
    - [Adjustment of RAM and Flash memory](https://devzone.nordicsemi.com/guides/short-range-guides/b/getting-started/posts/adjustment-of-ram-and-flash-memory)
    - [Introduction to Nordic nRF5 SDK and Softdevice](https://devzone.nordicsemi.com/guides/short-range-guides/b/getting-started/posts/introduction-to-nordic-nrf5-sdk-and-softdevice)

## First steps with nRF52840

### Following `Introduction to Nordic nRF5 SDK and Softdevice`

- It introduces the definition of SDK and SoftDevice
- It introduces the differences between SDK, Connect SDK and SDK for vertical applications (Mesh, Thread, ZigBee, ...)
- It recommends first to start with the SDK for simple examples, or to move to a vertical one if it is the real application

#### Downloading the SDK

There are two ways to download it:

- Human-friendly (using proposed *[Download nRF5 SDK here](https://www.nordicsemi.com/Software-and-tools/Software/nRF5-SDK/Download#infotabs)* link)
  - User has to select which SDK version and SoftDevice is wanted
  - Once download button is clicked, the site prepares a zip and let the download begins
  - Not very useful under a OCI image environment, where pure permanent links are expected
- Machine-friendly (using *[SDK (Software Development Kit)](https://developer.nordicsemi.com/)* link)
  - [nRF5_SDK_17.1.0_ddde560.zip](https://developer.nordicsemi.com/nRF5_SDK/nRF5_SDK_v17.x.x/nRF5_SDK_17.1.0_ddde560.zip)
  - [nRF5_SDK_17.1.0_offline_doc.zip](https://developer.nordicsemi.com/nRF5_SDK/nRF5_SDK_v17.x.x/nRF5_SDK_17.1.0_offline_doc.zip)

### Following `nRF52840 Dongle Programming Tutorial`

### Errors with different versions

### Using `nrfutil`

```bash
cd /workdir/blinky/pca10059/mbr/armgcc;
# Comment SDK_ROOT, which is pointing nowhere
make clean;
make;
nrfutil pkg generate --hw-version 52 --sd-req 0x00 --application-version 4 --application _build/nrf52840_xxaa.hex _build/dfu.zip;
nrfutil dfu usb-serial -pkg _build/dfu.zip -p /dev/ttyACM0 -b 115200;
# Led blinks, finally
```

## Evolution stopped here

As per an [official statement](https://devzone.nordicsemi.com/nordic/nordic-blog/b/blog/posts/nrf-connect-sdk-and-nrf5-sdk-statement), `nRF Connect SDK` should be used instead of `nRF SDK`.

This makes a non-sense to continue this path.

### Links saved for reference

- [VS Code + nRF SDK + nRF52840 (Website)](https://www.justinmklam.com/posts/2019/04/vscode-nrf52/): Explains SDK_ROOT usage
- [VS Code + nRF SDK + nRF52840 (GitHub)](https://github.com/justinmklam/nrf52-blinky-demo)
- [VS Code + Makefile for nrfutil](https://devzone.nordicsemi.com/f/nordic-q-a/75645/flashing-nrf52840-dongle-from-visual-studio-code-using-nrfjprog-as-in-the-makefile)
- [VS Code + SDK v15](https://github.com/gera-k/VSC-Nordic-example)
- [Good explanation of folders and files](https://devzone.nordicsemi.com/guides/short-range-guides/b/getting-started/posts/introduction-to-nordic-nrf5-sdk-and-softdevice): With nice images
- [Newer SoftDevice ID, not in nrfutil](https://devzone.nordicsemi.com/f/nordic-q-a/67731/sd-req-for-soft-device-s140-v7-2-0): `0x0101` for 7.2.0
  - [Complaints on GitHub](https://github.com/NordicSemiconductor/pc-nrfutil/issues/342): References to fixes
- [nRF52840 Dongle programming tutorial](https://devzone.nordicsemi.com/guides/short-range-guides/b/getting-started/posts/nrf52840-dongle-programming-tutorial): Good nrfutil examples
- [nRF52840 as a HID keyboard](https://devzone.nordicsemi.com/f/nordic-q-a/38175/nrf52-as-a-hid-keyboard)
- [Dongle based on oldie v15 kind of complete](https://jimmywongiot.com/2019/10/25/how-to-use-the-nrf52840-dongle-pca10059-as-development-board/)
