# Setup notes for nRF Connect SDK

- nRF52840 Dongle is also known as `PCA10059`.
- nRF5 Connect SDK references this board as `nrf52840dongle_nrf52840`

## Links from Nordic site

- [nRF52840 dongle Home](https://www.nordicsemi.com/Products/Development-hardware/nRF52840-Dongle/GetStarted)
- Get Started
  - [Getting started with the nRF52840 Dongle](https://infocenter.nordicsemi.com/index.jsp?topic=%2Fug_nrf52840_dongle%2FUG%2Fnrf52840_Dongle%2Fgetting_started.html)
  - Getting started guide
    - [Nordic Tools and downloads](https://infocenter.nordicsemi.com/topic/ug_getting_started/UG/common/nordic_tools.html?cp=1_0_1)
    - [Available protocols](https://infocenter.nordicsemi.com/topic/ug_getting_started/UG/gs/avail_protocols.html?cp=1_0_0)
    - [Software development](https://infocenter.nordicsemi.com/topic/ug_getting_started/UG/gs/develop_sw.html?cp=1_0_2)
- Relevant SDKs
  - [nRF Connect SDK](https://www.nordicsemi.com/Products/Development-software/nRF-Connect-SDK)