# NiceDoc

According base image `pandoc/ubuntu-latex:2.14`, it should work as:

```bash
docker run --rm \
  --volume "$(pwd):/data" \
  --user "$(id -u)":"$(id -g)" \
  registry.gitlab.com/laboratorio-iot/kb/hub/nicedoc:0.0.4 \
  README.md
```

So, in case of a local development, it is translated to:

```bash
docker run --rm \
  --volume "$(pwd):/data" \
  --user "$(id -u)":"$(id -g)" \
  nicedoc:local \
  README.md
```

## Beamer themes + colors

```plaintext
beamerauxtheme
beamercolorthemeowl
beamerdarkthemes
beamertheme-cuerna
beamertheme-detlevcm
beamertheme-epyt
beamertheme-focus
beamertheme-light
beamertheme-metropolis
beamertheme-npbt
beamertheme-phnompenh
beamertheme-pure-minimalistic
beamertheme-saintpetersburg
beamertheme-upenn-bc
beamerthemejltree
beamerthemenirma
beamerthemenord
```
