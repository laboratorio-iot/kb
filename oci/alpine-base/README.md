# Alpine base image

Official alpine with some pre-installed stuff to be used as a base for all our images, served as a small single-layer image.

## What has been added to plain Alpine image

1. Environment variables
   - `OS_AND_VERSION` to reflects current OS and version, used in `PS1`
   - `LANG`, `LANGUAGE` and `LC_ALL` are set to `en_US.UTF-8` for better internationalization
   - `PS1_A` to `PS1_E` are just the bits that compose `PS1`, for better understanding
   - `PS1` is set so is more verbose, printing user, host, time ...
2. Non-root user
   - User `user` and group `group` are created
   - Both user and group has id `1000`, which is the most common case.  
     This will reduce permissions errors under Linux/Mac environments, when mounting folders in the container and a specific mapping is not set.
   - Owner of `/workdir`
3. Generic working directory
   - Present as `/workdir` owned by `user:group`
   - Useful for generic workloads, leaving `/home/user` for user-specific things
4. Pre-installed packages
   - `bash`
   - `git`

## TL;DR; Usage

### Basic execution

User will be root and a bash session is launched under a fully isolated environment.

```bash
docker run --rm -it registry.gitlab.com/laboratorio-iot/kb/hub/alpine:latest
```

### Improved execution

Current folder is mounted as `/workdir` within the container.

Container user and group will be set by id, extracted from the user that executed the docker command. If this user and its default group ids are `1000`, then they will match with container default user (`user`) and group (`group`) respectively, reducing error surface regarding permissions under Linux/Mac host environments.

```bash
docker run --rm -it \
  -u "$(id -u):$(id -g)" \
  -v "$(pwd):/workdir" \
  registry.gitlab.com/laboratorio-iot/kb/hub/alpine:latest
```

## Authors

- CieNTi <cienti@cienti.com>
