# OCI images

This folder contains both mirrored and custom images.

## Mirrored images

Official images mirrored here to avoid DockerHub restrictions. It also serves as backup for future cases and repeatability.

- `registry.gitlab.com/laboratorio-iot/kb/hub/alpine:3.15`
- `registry.gitlab.com/laboratorio-iot/kb/hub/alpine-base:3.15`
- `registry.gitlab.com/laboratorio-iot/kb/hub/alpine-base:3.15-0.0.2`
- `registry.gitlab.com/laboratorio-iot/kb/hub/dive:0.10.0`
- `registry.gitlab.com/laboratorio-iot/kb/hub/hadolint:2.6.0-0-g7160964-alpine`
- `registry.gitlab.com/laboratorio-iot/kb/hub/ubuntu:20.04`
- `registry.gitlab.com/laboratorio-iot/kb/hub/ubuntu-base:20.04`
- `registry.gitlab.com/laboratorio-iot/kb/hub/ubuntu-base:20.04-0.0.2`
